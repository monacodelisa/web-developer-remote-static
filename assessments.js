const level = document.querySelectorAll(".level");
const popup = document.getElementById("pop-up");
const popupContent = document.getElementById("pop-up-content");
const backdrop = document.getElementById("backdrop");
const closePopup = document.getElementById("close-popup");

level.forEach((item) => {
  item.addEventListener("mouseenter", () => {
    item.innerText = "Select";
  });
});

level.forEach((item) => {
  item.addEventListener("mouseleave", () => {
    if (item.classList.contains("junior")) {
      item.innerText = "Junior";
    }
    if (item.classList.contains("mid")) {
      item.innerText = "Mid-Level";
    }
    if (item.classList.contains("senior")) {
      item.innerText = "Senior";
    }
  });
});

level.forEach((item) => {
  item.addEventListener("click", () => {
    popup.classList.add("show");
    backdrop.classList.add("show");
    let classes = item.classList;
    let skill = classes[1];
    let skillCap = skill.charAt(0).toUpperCase() + skill.slice(1);
    if (
      skill === "html" ||
      skill === "css" ||
      skill === "sass" ||
      skill === "js" ||
      skill === "ts" ||
      skill === "php" ||
      skill === "go" ||
      skill === "git"
    ) {
      skillCap = skillCap.toUpperCase();
    }
    if (skill === "tailwind") {
      skillCap = skillCap + " CSS";
    }
    if (skill === "jquery") {
      skillCap = "jQuery";
    }
    if (skill === "react" || skill === "vue" || skill === "node") {
      skillCap = skillCap + ".JS";
    }
    if (skill === "csharp") {
      skillCap = "C#";
    }
    if (skill === "cplusplus") {
      skillCap = "C++";
    }
    if (skill === "dot-net") {
      skillCap = ".NET";
    }
    if (classes.contains("junior")) {
      popupContent.innerHTML = `<h1> Junior ${skillCap} Assessment</h1>`;
    } else if (classes.contains("mid")) {
      popupContent.innerHTML = `<h1> Mid-Level ${skillCap} Assessment</h1>`;
    } else if (classes.contains("senior")) {
      popupContent.innerHTML = `<h1> Senior ${skillCap} Assessment</h1>`;
    } else {
      popupContent.innerHTML = "<h1>Try Again</h1>";
    }
  });
});

closePopup.addEventListener("click", () => {
  popup.classList.remove("show");
  backdrop.classList.remove("show");
});

backdrop.addEventListener("click", () => {
  popup.classList.remove("show");
  backdrop.classList.remove("show");
});

// // Slick Carousel Plugin
$(document).ready(function () {
  $(".assessments-container").slick({
    slidesToScroll: 1,
    speed: 400,
    arrows: true,
    variableWidth: true,
  });
});
$(".assessments-container").slick("setPosition");
$(window).on("resize orientationchange", function () {
  $(".assessments-container").slick("resize");
});

$("html").animate({ scrollTop: 0 }, "fast");
